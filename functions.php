<?php

function skinny_child_enqueue_styles() {
    $parent_style = 'skinny-ninjah';

    wp_enqueue_script( $parent_style, get_stylesheet_directory_uri() . '/dist/app.js', [], THULANI_VER );
}
add_action( 'wp_enqueue_scripts', 'skinny_child_enqueue_styles' );


//require_once( get_stylesheet_directory_uri() . '/inc/post-meta.php' );
get_template_part('inc/post-meta');





function skinny_ninjah_child_widgets_init() {
    register_sidebar( array(
        'name'          => esc_html__( 'Footer One', 'skinny-ninjah' ),
        'id'            => 'footer-one',
        'description'   => esc_html__( 'Add widgets here.', 'skinny-ninjah' ),
        'before_widget' => '<section id="%1$s" class="ninjah-widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name'          => esc_html__( 'Footer Two', 'skinny-ninjah' ),
        'id'            => 'footer-two',
        'description'   => esc_html__( 'Add widgets here.', 'skinny-ninjah' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name'          => esc_html__( 'Footer Three', 'skinny-ninjah' ),
        'id'            => 'footer-three',
        'description'   => esc_html__( 'Add widgets here.', 'skinny-ninjah' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'skinny_ninjah_child_widgets_init' );

