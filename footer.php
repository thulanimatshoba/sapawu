<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Skinny_Ninjah
 */

?>

	</div><!-- #content -->

<div class="footer-wrapper">
	<footer id="colophon" class="site-footer">
        <div class="footer-top">
            <div class="uk-container uk-padding">
                <div class="uk-child-width-expand@s" uk-grid>
                    <div><?php if (! dynamic_sidebar('footer-one') ): endif; ?></div>
                    <div><?php if (! dynamic_sidebar('footer-two') ): endif; ?></div>
                    <div><?php if (! dynamic_sidebar('footer-three') ): endif; ?></div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="uk-container uk-padding-small">
                <?php _e('All rights reserved &copy;');?> <?php echo date("Y"); echo " "; bloginfo('name'); ?>
            </div>
        </div><!-- .uk-container -->
	</footer><!-- #colophon -->
</div>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
