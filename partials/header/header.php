<div class="uk-offcanvas-content">

    <header id="masthead" class="site-header" data-uk-sticky="{clsinactive: 'uk-sticky-disabled', top: -90, animation: 'uk-animation-slide-top-medium'}" style="background-image: url(<?php header_image(); ?>); background-repeat: no-repeat; background-size: cover; background-position: center center;">

    <div class="uk-container uk-navbar">
        <div class="uk-navbar-left">
            <?php get_template_part('partials/header/logo'); ?>
        </div>
        
        <nav id="site-navigation" class="main-navigation uk-navbar-right">
            <button class="uk-navbar-toggle menu-toggle uk-hidden@s" uk-navbar-toggle-icon href="#" uk-toggle="target: #offcanvas-push"></button>
            <?php get_template_part('partials/header/menu/header', 'menu'); ?>
        </nav><!-- #site-navigation -->
    </div>
    </header><!-- #masthead -->

<?php get_template_part( 'partials/header/menu/offcanvas', 'menu' ); ?>