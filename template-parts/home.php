<?php
/**
 * Template Name: Home Page
 *
 * This is the template that the home page.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Skinny_Ninjah
 */

    $objectives_title = carbon_get_the_post_meta( 'objectives_title' );
    $mission_title = carbon_get_the_post_meta( 'mission_title'  );
    $sponsor_title = carbon_get_the_post_meta( 'sponsor_title' );

    $missions = carbon_get_the_post_meta( 'skinny_ninjah_mission' ); 
    $objectives = carbon_get_the_post_meta( 'skinny_ninjah_objectives' );

    $contact_title = carbon_get_the_post_meta( 'contact_title' );
    $contact_info = carbon_get_the_post_meta( 'contact_info' );

    get_header(); 
?>

    <!-- <div class="go-down">
        <a href="#content">
            <i class="fa fa-angle-down fa-3x"></i>
        </a>
    </div> -->

   
    <div class="home-slider">
        <?php if (!dynamic_sidebar('home-slider')): endif; ?>
    </div>

    <div id="primary" class="content-area">
        <main id="main" class="site-main">

            <?php
            while ( have_posts() ) :
                the_post();

                get_template_part( 'template-parts/pages/content', 'home' );

            endwhile; // End of the loop.
            ?>

            <div class="latest-posts uk-margin-large-top uk-margin-large-bottom">
                <div class="uk-container">
                    <h2 class="widget-title"><?php _e('Latest Posts'); ?> </h2>
                    <div class="uk-child-width-expand@s" uk-scrollspy="cls: uk-animation-fade; target: > .damn; delay: 500; repeat: false" uk-grid>
                    <?php
                    if (is_page()) {
                        $cat = get_cat_ID('Uncategorized'); //use page title to get a category ID
                        $posts = get_posts("cat=$cat&showposts=3");

                        if ($posts) {
                            foreach ($posts as $post):
                                setup_postdata($post); ?>
                                <div class="damn">
                                    <div class="other-story-header">
                                        <?php if (function_exists('add_theme_support')) {
                                            if (has_post_thumbnail()) { ?>
                                                <a href="<?php the_permalink(); ?>"
                                                   title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail('featured-thumb'); ?></a>
                                            <?php }
                                        } ?>

                                        <div class="uk-meta">
                                            <span><?php the_time('jS<br> F') ?></span><br>
                                            <span class="uk-margin-small-right"><i class="fa fa-eye"></i> <?php //echo getPostViews(get_the_ID()); ?></span>
                                            <span class="post-comments"><i class="fa fa-comments"> <?php comments_number(0, 1, '%'); ?></i></span>
                                        </div>
                                    </div>

                                    <div class="other-story-content">
                                        <?php if (strlen(the_title('', '', FALSE)) > 16) {
                                            $title_short = substr(the_title('', '', FALSE), 0, 16);
                                            preg_match('/^(.*)\s/s', $title_short, $matches);
                                            if ($matches[1]) $title_short = $matches[1];
                                            $title_short = $title_short . ' ...';
                                        } else {
                                            $title_short = the_title('', '', FALSE);
                                        } ?>

                                        <a class="the-title" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><h4><?php the_title(); ?></h4></a>
                                        <p><?php //echo wp_trim_excerpt();?></p>
                                    </div>

                                </div>
                            <?php endforeach;

                        wp_reset_postdata(); 
                        }
                    }
                    ?>
                    </div>
                </div>
            </div>
            

            <div class="mission-vision">
                <div class="uk-container uk-padding serv-container">
                
                    <div class="uk-child-width-expand@s" uk-grid uk-scrollspy="cls: uk-animation-slide-bottom; target: .uk-padding-small; delay: 300; repeat: true">
                        <?php
                        echo '<div>';
                            if ($objectives_title): ?>
                                <h2><?php echo _e($objectives_title); ?></h2>
                            <?php endif; 

                            echo '<ul>';
                            foreach ( $objectives as $objective ) {
                                echo '<li class="uk-padding-small">';
                                echo '<span>' . $objective['objective_descr'] . '</span>';
                                echo '</li>';
                            }
                            echo '</ul>';
                            echo '</div>';
                            
                            echo '<div>';
                            if ($mission_title): ?>
                                <h2 class="gg"><?php echo _e($mission_title); ?></h2>
                            <?php endif; 

                            echo '<ul>';
                            foreach ( $missions as $mission ) {
                                echo '<li class="uk-padding-small">';
                                echo '<span>' . $mission['mission_descr'] . '</span>';
                                echo '</li>';
                            }
                            echo '</ul>';
                            echo '</div>';
                        
                        ?>
                    </div>    
                </div>
            </div>

            <div class="contact-us" uk-scrollspy="cls: uk-animation-slide-top-medium; target: .contact-form; delay: 300; repeat: true" >
                <div class="uk-container">
                <?php
                    if ($contact_title): ?>
                    <div class="title-wrap">
                        <h1 class="uk-text-center uk-padding-small"><?php echo ($contact_title); ?></h1>
                        </div>
                    <?php endif; 

                    if ( $contact_info ): ?>
                    <div class="contact-form">
                        <?php echo apply_filters( 'the_content', carbon_get_the_post_meta( 'contact_info' ) ); ?>
                    </div>
                    <?php endif; ?>

                </div>
            </div>

            <div class="sponsors">
                <div class="uk-container uk-padding serv-container">
                    <?php
                    if ($sponsor_title): ?>
                        <h1 class="uk-text-center uk-padding"><?php echo _e($sponsor_title); ?></h1>
                    <?php endif; 
                    
                            $slides = carbon_get_the_post_meta( 'skinny_ninjah_slides' ); ?>
                            <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slider="autoplay: true">
                    
                                <?php
                                    echo '<ul class="uk-slider-items uk-child-width-1-2 uk-child-width-1-3@s uk-child-width-1-4@m" uk-grid uk-scrollspy="cls: uk-animation-fade; target: li; delay: 500; repeat: false">';
                                    if ( $slides ) {
                                        foreach ( $slides as $slide ) {
                                            echo '<li class="uk-text-center">';
                                            echo wp_get_attachment_image( $slide['image'], 'logo-carousel' );
                                            echo '</li>';
                                        }
                                    }
                                    echo '</ul>';

                                ?>
                                <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                                <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>
                            </div>
                          
                </div>
            </div>


        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();