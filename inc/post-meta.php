<?php
/**
 * Created by PhpStorm.
 * User: thulani-matshoba
 * Date: 2019-03-24
 * Time: 00:55
 */


use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action( 'carbon_fields_register_fields', 'skinny_ninjah_post_metas' );
function skinny_ninjah_post_metas() {

    Container::make( 'post_meta', __( 'Objectives', 'skinny-ninjah' ) )
    ->show_on_page('home')
    ->add_fields( array(
        Field::make('text', 'objectives_title'),
        Field::make( 'complex', 'skinny_ninjah_objectives', '' )
            ->set_layout( 'tabbed-vertical' )
            ->add_fields( array(
        Field::make( 'textarea', 'objective_descr', 'Description' ),
    ) )
    ->set_header_template( '
            <% if (objective_descr) { %>
                <%- objective_descr %>
            <% } else { %>
                empty
            <% } %> ' 
        ),
    ) );

    Container::make( 'post_meta', __( 'Mission & Vision', 'skinny-ninjah' ) )
    ->show_on_page('home')
    ->add_fields( array(
        Field::make('text', 'mission_title'),
        Field::make( 'complex', 'skinny_ninjah_mission', '' )
            ->set_layout( 'tabbed-vertical' )
            ->add_fields( array(
                Field::make( 'textarea', 'mission_descr', 'Description' ),
            ) )
            ->set_header_template( '
                <% if (mission_descr) { %>
                    <%- mission_descr %>
                <% } else { %>
                    empty
                <% } %> ' 
            ),
    ) );

    Container::make( 'post_meta', __( 'Contacts', 'skinny-ninjah' ) )
    ->show_on_page('home')
    ->add_fields(array(
        Field::make('text', 'contact_title')->set_width(30),
        Field::make('rich_text', 'contact_info')->set_width(30),
        Field::make( 'map', 'crb_company_location', 'Location' )
            ->set_position( -33.9142688, 18.0956097,9 )
            ->set_help_text( 'drag and drop the pin on the map to select location or search for the location' ),
    ));

    Container::make( 'post_meta', __( 'Sponsors', 'skinny-ninjah' ) )
    ->show_on_page('home')
    ->add_fields( array(
        Field::make('text', 'sponsor_title'),
        Field::make( 'complex', 'skinny_ninjah_slides', 'Team' )
            ->set_layout( 'tabbed-horizontal' )
            ->add_fields( array(
                Field::make( 'text', 'title', 'Sponsor Name' ),
                Field::make( 'color', 'title_color', 'Sponsor Title Color' ),
                Field::make( 'image', 'image', 'Sponsor Image' ),
            ) )
            ->set_header_template( '
                <% if (title) { %>
                    <%- title %>
                <% } else { %>
                    empty
                <% } %> ' 
            ),
    ) );


    
}

add_action( 'after_setup_theme', 'skinny_ninjah_loads' );



function skinny_ninjah_loads() {
    \Carbon_Fields\Carbon_Fields::boot();
}